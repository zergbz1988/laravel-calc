## Installing

##### In Laravel project:

```
composer require zergbz1988/laravel-calc --dev

php artisan vendor:publish --provider="Zergbz1988\Calc\CalcServiceProvider"
```

## Configuring

##### In app/config/calc.php you can change 
```
'calcClass' => '{YOUR_CLASS}'
```

`YOUR_CLASS` must implement `Zergbz1988\Calc\Interfaces\Calc`

## Testing

##### Use <a href="https://docs.gitlab.com/ee/ci/runners/README.html">gitlab-runner</a> (download, install and register) and <a href="https://www.docker.com/">docker</a> to run test locally on php:5.4 to php:7.2:
```
gitlab-runner exec docker test:5.4
gitlab-runner exec docker test:5.5
gitlab-runner exec docker test:5.6
gitlab-runner exec docker test:7.0
gitlab-runner exec docker test:7.1
gitlab-runner exec docker test:7.2
```

##### or use gitlab-runner through <a href="https://gitlab.com/zergbz1988/laravel-calc/pipelines">Gitlab pipelines</a> 

P.S. Php 5.3 testing was removed due to composer memory issue (php -d memory_limit=-1 composer.phar didn't help).

## How to use

##### In browser: 

```
http://your-app-host/calc?input=3+2
```

##### In console: 

```
php artisan calc:run 3+2
```



